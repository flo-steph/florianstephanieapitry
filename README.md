Site internet ayant pour but la gestion des utilisateurs.

#Installation du serveur front
##Se placer dans le répertoire Server :
npm install

##Démarrage :
node server.js
ou
nodemon server.js

##Accès
http://localhost:8080/user

#Installation du serveur back
##Se placer dans le répertoire API :
npm install

##Démarrage :
node api.js
ou
nodemon api.js

##Fichier base de données
./users.json

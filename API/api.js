const express = require('express');
const fs = require('fs');
const bodyParser = require('body-parser');

const app = express();
const port = 3000;
var tabUserJSON = [];

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//Ecrit sur le fichier users.json le contenu du tableau d'utilisateurs
function saveJson() {
    fs.writeFileSync('users.json', JSON.stringify(tabUserJSON), 'utf8');
}

//Vérifie l'existence du fichier users.json et le crée s'il n'existe pas
if (!fs.existsSync('users.json')) {
    saveJson();
} else {
    //Récupère dans le tableau toutes les informations contenues dans le fichier users.json
    tabUserJSON = fs.readFileSync('users.json', 'utf8');
    try {
        tabUserJSON = JSON.parse(tabUserJSON);
    //Affecte un tableau vide si le contenu du fichier users.json n'est pas au bon format
    } catch (error) {
        tabUserJSON = [];
    }
}

//Enregistre un nouvel utilisateur
app.post('/user/new', (req, res) => {
    const user = req.body;
    user.name = user.name.trim();
    user.id = user.name + Date.now();
    tabUserJSON.push(user);
    //Ecrase le fichier JSON existant avec les données mises à jour
    saveJson();
    res.send();
});

//Renvoie au serveur front le tableau d'utilisateurs
app.get('/user', (req, res) => {
    res.send(tabUserJSON);
});

//Supprime l'utilisateur du tableau en fonction de son nom et modifie le fichier json
//TODO (si ok) : suppression en fonction de l'id pour ne pas effacer toutes les personnes qui ont le même nom
app.delete('/user/:name', (req, res) => {
    const deletedName = req.params.name;
    //Garde tous les éléments du tableau qui n'ont pas le même nom que celui de l'utilisateur à supprimer
    tabUserJSON = tabUserJSON.filter(user => user.name !== deletedName);
    saveJson();
    res.send();
});

//Récupère l'objet utilisateur dans le tableau en fonction de son id et le renvoie au serveur front
app.get('/user/:id', (req,res) => {
    let userId = req.params.id;
    userId = userId.trim();
    let editedUser;
    for (i=0; i<tabUserJSON.length; i++) {
        if (tabUserJSON[i].id === userId) {
            editedUser = tabUserJSON[i];
            break;
        }
    }
    res.send(editedUser);
});

//Modifie dans le tableau les données de l'utilisateur et met à jour le fichier json
app.put('/user/edit', (req, res) => {
    const editedUser = req.body;
    for (i=0; i<tabUserJSON.length; i++) {
        if (tabUserJSON[i].id === editedUser.id) {
            tabUserJSON[i] = editedUser;
            break;
        }
    }
    saveJson();
    res.send(tabUserJSON);
});

app.listen(port);
console.log('Server started : http://localhost:' + port);
const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');

const app = express();
const port = 8080;
const apiUrl = 'http://localhost:3000';

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.set('view-engine', 'ejs');

//Renvoie vers la page qui affiche les utilisateurs
app.get('/user', (req, res) => {
        //Récupère le tableau d'utilisateurs du côté de l'API
    axios.get(apiUrl + '/user')
        .then (response => {
            const users = response.data;
            res.render('user.ejs', { users })
        })
});

//Renvoie vers la page du formulaire
app.get('/form/', (req, res) => {
    res.render('form.ejs');
});

//Envoie les données du formulaire à l'API
app.post('/form/', (req, res) => {
    const dataForm = req.body;
    axios.post(apiUrl + '/user/new', dataForm);
    res.render('valid.ejs');
});

//Récupère le nom de l'utilisateur à supprimer, l'envoie à l'API et reçoit le tableau d'utilisateurs mis à jour
app.post('/user', (req, res) => {
    const deletedName = req.body.deletedName;
    axios.delete(apiUrl + '/user/' + deletedName)
    .then (response => {
        axios.get(apiUrl + '/user')
        .then(response => {
            const users = response.data;
            res.render('user.ejs', { users })
        })
    })
});

//Envoie l'id de l'utilisateur à modifier à l'API, récupère cet objet utilisateur et affiche ses données sur la page d'édition
app.post('/useredit', (req, res) => {
    let editedId = req.body.userId;
    axios.get(apiUrl + '/user/' + editedId)
    .then (response => {
        const user = response.data;
        res.render('useredit.ejs', { user });
    })
});

//Envoie les informations modifiées de l'utilisateur à l'API et affiche une page statique de confirmation
app.post('/editeduser', (req, res) => {
    const dataForm = req.body;
    axios.put(apiUrl + '/user/edit', dataForm);
    res.render('valid.ejs');
});

app.listen(port);
console.log('Server started : http://localhost:' + port);